# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .work import WorkCenter


def register():
    Pool.register(
        WorkCenter,
        module='production_work_center_code', type_='model')

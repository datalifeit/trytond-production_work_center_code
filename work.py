# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class WorkCenter(metaclass=PoolMeta):
    __name__ = 'production.work.center'

    code = fields.Char('Code')

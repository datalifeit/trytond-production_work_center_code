====================================
Production Work Center Code Scenario
====================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules


Install production_work_center_code::

    >>> config = activate_modules('production_work_center_code')
